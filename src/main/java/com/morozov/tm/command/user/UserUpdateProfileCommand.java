package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserExistException;
import com.morozov.tm.util.ConsoleHelper;
import com.morozov.tm.util.MD5Hash;

public class UserUpdateProfileCommand extends AbstractCommand {
    @Override
    final public String getName() {
        return "user-update-profile";
    }

    @Override
    final public String getDescription() {
        return "Update user profile";
    }

    @Override
    final public void execute() {
        final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelper.writeString("Введите старый пароль");
        final String oldPassword = ConsoleHelper.readString();
        if (MD5Hash.getHash(oldPassword).equals(currentUser.getPasswordHash())) {
            ConsoleHelper.writeString("Введите новое имя пользователя");
            final String newUserName = ConsoleHelper.readString();
            ConsoleHelper.writeString("Введите новый пароль");
            final String newUserPassword = ConsoleHelper.readString();
            try {
                serviceLocator.getUserService().updateUserProfile(currentUser.getId(),newUserName,newUserPassword);
                ConsoleHelper.writeString("Профиль обновлен");
            } catch (final StringEmptyException e) {
                ConsoleHelper.writeString("Веденные строки не могут быть пустыми");
            } catch (final UserExistException e) {
                ConsoleHelper.writeString("Пользователь с таким именем уже существует");
            }
        }
    }
}
