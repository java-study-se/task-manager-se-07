package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelper;

public class UserLoginCommand extends AbstractCommand {
    @Override
    final public String getName() {
        return "user-login";
    }

    @Override
    final public String getDescription() {
        return "User authorisation";
    }

    @Override
    final public void execute() {
        ConsoleHelper.writeString("Введите логин");
        final String login = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите пароль");
        final String password = ConsoleHelper.readString();
        try {
            final User authoriseUser = serviceLocator.getUserService().loginUser(login,password);
            if(authoriseUser != null) {
                serviceLocator.setCurrentUser(authoriseUser);
                ConsoleHelper.writeString("Вы вошли под учетной записью " + authoriseUser.getLogin());
            } else {
                ConsoleHelper.writeString("Некорректный пароль пользователя");
            }
        } catch (final UserNotFoundException e) {
            ConsoleHelper.writeString("Пользователь не найден");
        } catch (final StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        }
    }
}
