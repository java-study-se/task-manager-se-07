package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelper;

public class UserLogoutCommand extends AbstractCommand {
    @Override
    final public String getName() {
        return "user-logout";
    }

    @Override
    final public String getDescription() {
        return "Close current user session";
    }

    @Override
    final public void execute() {
        final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelper.writeString(String.format("Сессия пользователя %s закрыта", currentUser.getLogin()));
        serviceLocator.setCurrentUser(null);
    }
}
