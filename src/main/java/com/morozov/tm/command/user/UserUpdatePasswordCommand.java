package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.util.ConsoleHelper;
import com.morozov.tm.util.MD5Hash;

public class UserUpdatePasswordCommand extends AbstractCommand {
    @Override
    final public String getName() {
        return "user-password-update";
    }

    @Override
    final public String getDescription() {
        return "Update user password";
    }

    @Override
    final public void execute() {
        final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelper.writeString("Введите старый пароль");
        final String oldPassword = ConsoleHelper.readString();
        if (MD5Hash.getHash(oldPassword).equals(currentUser.getPasswordHash())) {
            ConsoleHelper.writeString("Введите новый пароль");
            final String newPassword = ConsoleHelper.readString();
            try {
                serviceLocator.getUserService().updateUserPassword(currentUser.getId(), newPassword);
                ConsoleHelper.writeString("Пароль пользователя " + currentUser.getLogin() + " изменен");
            } catch (final StringEmptyException e) {
                ConsoleHelper.writeString("Пароль не может быть пустым");
            }
        }
    }
}
