package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelper;

public class UserShowCommand extends AbstractCommand {
    @Override
    final public String getName() {
        return "user-show";
    }

    @Override
    final public String getDescription() {
        return "Show current user";
    }

    @Override
    final public void execute() {
        final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelper.writeString("Имя текущего пользователя: " + currentUser.getLogin());
        ConsoleHelper.writeString("ID текущего пользователя: " + currentUser.getId());
        ConsoleHelper.writeString("Права текущего пользователя: " + currentUser.getRole().getDisplayName());
    }
}
