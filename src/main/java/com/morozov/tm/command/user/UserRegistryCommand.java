package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserExistException;
import com.morozov.tm.util.ConsoleHelper;

public class UserRegistryCommand extends AbstractCommand {
    @Override
    final public String getName() {
        return "user-reg";
    }

    @Override
    final public String getDescription() {
        return "Registry new user";
    }

    @Override
    final public void execute() {
        ConsoleHelper.writeString("Введите имя пользователя");
        final String login = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите пароль пользователя");
        final String password = ConsoleHelper.readString();
        try {
            serviceLocator.getUserService().registryUser(login,password);
            ConsoleHelper.writeString("Пользователь с логином " + login + " зарегистрирован");
        } catch (final UserExistException e) {
            ConsoleHelper.writeString("Такой пользователь уже существует");
        } catch (final StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        }
    }
}
