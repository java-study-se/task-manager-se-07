package com.morozov.tm.command.system;

import com.jcabi.manifests.Manifests;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.util.ConsoleHelper;

public class AboutCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "About program";
    }

    @Override
    public void execute() {
        ConsoleHelper.writeString("Информация о приложении");
        final String version = Manifests.read("BuildNumber");
        ConsoleHelper.writeString("Версия сборки: " + version);
    }
}
