package com.morozov.tm.command.system;


import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.util.ConsoleHelper;

public class HelpCommand extends AbstractCommand {

    @Override
    final public String getName() {
        return "help";
    }

    @Override
    final public String getDescription() {
        return "Show all command";
    }

    @Override
    final public void execute() {
        for (final AbstractCommand command: serviceLocator.getCommandList()) {
            ConsoleHelper.writeString(String.format("%s: %s", command.getName(), command.getDescription()));
        }
    }
}
