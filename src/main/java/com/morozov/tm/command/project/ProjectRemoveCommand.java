package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelper;

public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    final public String getName() {
        return "project-remove";
    }

    @Override
    final public String getDescription() {
        return "Remove selected project";
    }

    @Override
    final public void execute() {
        final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelper.writeString("Введите ID проекта для удаления");
        final String idDeletedProject = ConsoleHelper.readString();
        try {
            if (serviceLocator.getProjectService().deleteProject(currentUser.getId(), idDeletedProject)) {
                ConsoleHelper.writeString("Проект с ID: " + idDeletedProject + " удален");
                ConsoleHelper.writeString("Удаление задач с ID проекта: " + idDeletedProject);
                serviceLocator.getTaskService().deleteAllTaskByProjectId(currentUser.getId(), idDeletedProject);
            } else {
                ConsoleHelper.writeString("Проекта с данным ID не существует");
            }
        } catch (final StringEmptyException e) {
            ConsoleHelper.writeString("ID не может быт пустым");
        }
    }
}
