package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Project;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelper;

import java.util.List;

public class ProjectListCommand extends AbstractCommand {
    @Override
    final public String getName() {
        return "project-list";
    }

    @Override
    final public String getDescription() {
        return "Show all projects";
    }

    @Override
    final public void execute() {
        final User currentUser = serviceLocator.getCurrentUser();
        try {
            final List<Project> projectList = serviceLocator.getProjectService().getAllProjectByUserId(currentUser.getId());
            ConsoleHelper.writeString("Список проектов пользователя " +currentUser.getLogin()+ " :");
            for (int i = 0; i < projectList.size(); i++) {
                ConsoleHelper.writeString(String.format("%d: %s", i, projectList.get(i).toString()));
            }
        } catch (final RepositoryEmptyException e) {
            ConsoleHelper.writeString("Список проектов пуст");
        }
    }
}
