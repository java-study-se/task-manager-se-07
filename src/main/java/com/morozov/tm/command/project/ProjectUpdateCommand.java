package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelper;

import java.text.ParseException;

public class ProjectUpdateCommand extends AbstractCommand {
    @Override
    final public String getName() {
        return "project-update";
    }

    @Override
    final public String getDescription() {
        return "Update selected project";
    }

    @Override
    final public void execute() {
        final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelper.writeString("Введите ID проекта для изменения");
        final String updateProjectId = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новое имя проекта");
        final String updateProjectName = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новое описание проекта");
        final String updateProjectDescription = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новую дату начала проекта в формате DD.MM.YYYY");
        final String startUpdateDate = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новую дату окончания проекта в формате DD.MM.YYYY");
        final String endUpdateDate = ConsoleHelper.readString();
        try {
            serviceLocator.getProjectService().updateProject(currentUser.getId(),  updateProjectId, updateProjectName, updateProjectDescription, startUpdateDate, endUpdateDate);
            ConsoleHelper.writeString("Проект изменен");
        } catch (final RepositoryEmptyException e) {
            ConsoleHelper.writeString("Список проектов пуст");
        } catch (final StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        } catch (final ParseException e) {
            ConsoleHelper.writeString("Неверный формат даты. Введите дату в формате DD.MM.YYYY");
        }
    }
}
