package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Project;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelper;

import java.text.ParseException;

public class ProjectCreateCommand extends AbstractCommand {
    @Override
    final public String getName() {
        return "project-create";
    }

    @Override
    final public String getDescription() {
        return "Create project";
    }

    @Override
    final public void execute() {
        ConsoleHelper.writeString("Введите имя нового проекта");
        final String projectName = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите описание нового проекта");
        final String projectDescription = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите дату начала нового проекта в формате DD.MM.YYYY");
        final String startProjectDate = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите дату окончания нового проекта в формате DD.MM.YYYY");
        final String endProjectDate = ConsoleHelper.readString();
        final User currentUser = serviceLocator.getCurrentUser();
        try {
            final Project addedProject = serviceLocator.getProjectService().addProject(currentUser.getId(), projectName, projectDescription, startProjectDate, endProjectDate);
            ConsoleHelper.writeString("Добавлен проект: " + addedProject.toString());
        } catch (final StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        } catch (final ParseException e) {
            ConsoleHelper.writeString("Неверный формат даты. Введите дату в формате DD.MM.YYYY");
        }
    }
}
