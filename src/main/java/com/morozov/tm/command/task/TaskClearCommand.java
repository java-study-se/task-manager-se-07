package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.util.ConsoleHelper;

public class TaskClearCommand extends AbstractCommand {
    @Override
    final public String getName() {
        return "task-clear";
    }

    @Override
    final public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    final public void execute() {
        serviceLocator.getTaskService().clearTaskList();
        ConsoleHelper.writeString("Список задач очищен");
    }
}
