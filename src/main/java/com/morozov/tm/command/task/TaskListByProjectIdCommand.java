package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelper;

import java.util.List;

public class TaskListByProjectIdCommand extends AbstractCommand {
    @Override
    final public String getName() {
        return "task-list-id";
    }

    @Override
    final public String getDescription() {
        return "Show all task by selected project";
    }

    @Override
    final public void execute() {
        final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelper.writeString("Введите ID проекта");
        final String projectId = ConsoleHelper.readString();
        try {
            final List<Task> taskList = serviceLocator.getTaskService().getAllTaskByProjectId(currentUser.getId(), projectId);
            ConsoleHelper.writeString("Список задач по проекту с ID: " + projectId);
            for (int i = 0; i < taskList.size(); i++) {
                ConsoleHelper.writeString(String.format(("%d: %s"), i, taskList.get(i).toString()));
            }
        } catch (final StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        } catch (final RepositoryEmptyException e) {
            ConsoleHelper.writeString("Список задач пуст");
        }
    }
}
