package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelper;

import java.text.ParseException;

public class TaskCreateCommand extends AbstractCommand {
    @Override
    final public String getName() {
        return "task-create";
    }

    @Override
    final public String getDescription() {
        return "Create task";
    }

    @Override
    final public void execute() {
        final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelper.writeString("Введите имя новой задачи");
        final String taskName = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите описание новой задачи");
        final String taskDescription = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите дату начала новой задачи в формате DD.MM.YYYY");
        final String startTaskDate = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите дату окончания новой задачи в формате DD.MM.YYYY");
        final String endTaskDate = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите ID проекта задачи");
        final String projectId = ConsoleHelper.readString();
        try {
            final Task task = serviceLocator.getTaskService().addTask(currentUser.getId(), taskName, taskDescription, startTaskDate, endTaskDate, projectId);
            ConsoleHelper.writeString("Добавлена задача " + task.toString());
        } catch (final StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        } catch (final ParseException e) {
            ConsoleHelper.writeString("Неверный формат даты. Введите дату в формате DD.MM.YYYY");
        }
    }
}
