package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelper;

public class TaskRemoveCommand extends AbstractCommand {
    @Override
    final public String getName() {
        return "task-remove";
    }

    @Override
    final public String getDescription() {
        return "Remove selected task";
    }

    @Override
    final public void execute() {
        final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelper.writeString("Введите порядковый номер задачи для удаления");
        final String idDeletedTask = ConsoleHelper.readString();
        try {
            if (serviceLocator.getTaskService().deleteTask(currentUser.getId(), idDeletedTask)) {
                ConsoleHelper.writeString("Задача с порядковым номером " + idDeletedTask + " удален");
            }
            ConsoleHelper.writeString("Задачи с данным ID не существует");
        } catch (final StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        }
    }
}
