package com.morozov.tm.command;

import com.morozov.tm.api.IServiceLocator;


public abstract class AbstractCommand {
    protected IServiceLocator serviceLocator;

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute();
}
