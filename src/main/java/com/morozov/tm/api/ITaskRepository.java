package com.morozov.tm.api;

import java.util.List;

public interface ITaskRepository<T> {
    List<T> findAll();

    List<T> findAllByProjectIdUserId(String userId, String projectId);

    List<T> findAllByUserId(String userId);

    T findOneByUserId(String userId, String id);

    T findOne(String id);

    void persist(String id, T writeEntity);

    void merge(String id, T updateEntity);

    void remove(String id);

    void removeAll();

    List<T> getAllTaskByProjectId(String projectId);

    void deleteAllTaskByProjectId(String userId, String projectId);
}
