package com.morozov.tm.api;


import java.util.List;

public interface IProjectRepository<T> {
    List<T> findAll();

    T findOne(String id);

    List<T> findAllByUserId(String userId);

    T findOneByUserId(String userId, String id);

    void persist(String id, T writeEntity);

    void merge(String id, T updateEntity);

    void remove(String id);

    void removeAll();
}
