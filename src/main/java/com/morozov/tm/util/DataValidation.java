package com.morozov.tm.util;

import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;

import java.util.List;

public class DataValidation {

    public static void checkEmptyRepository(final List repository) throws RepositoryEmptyException {
        if (repository.isEmpty()) throw new RepositoryEmptyException();
    }
    public static void checkEmptyString(final String... strings) throws StringEmptyException {
        for (String string: strings
             ) {
            if (string.isEmpty())throw new StringEmptyException();
        }
    }
}
