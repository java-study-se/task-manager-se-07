package com.morozov.tm.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ConsoleHelper {

    public static final String DATA_FORMAT = "dd.MM.yyyy";

    //чтение строки с консоли
    public static String readString() {
        String enterString = "";
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            enterString = bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return enterString;
    }

    //вывод строки в консоль
    public static void writeString(final String string) {
        System.out.println(string);
    }

    public static Date formattedData(String data) throws ParseException {
        final SimpleDateFormat format = new SimpleDateFormat(DATA_FORMAT);
        final Date date = format.parse(data);
        return date;
    }
}
