package com.morozov.tm.service;

import com.morozov.tm.command.*;
import com.morozov.tm.command.project.*;
import com.morozov.tm.command.system.AboutCommand;
import com.morozov.tm.command.system.HelpCommand;
import com.morozov.tm.command.task.*;
import com.morozov.tm.command.user.*;
import com.morozov.tm.exception.CommandCorruptException;
import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.repository.ProjectRepository;
import com.morozov.tm.repository.TaskRepository;
import com.morozov.tm.entity.User;
import com.morozov.tm.repository.UserRepository;
import com.morozov.tm.util.ConsoleHelper;

import java.util.*;

public final class Bootstrap implements IServiceLocator {

    final private ProjectService projectService = new ProjectService(new ProjectRepository());
    final private TaskService taskService = new TaskService(new TaskRepository());
    final private UserService userService = new UserService(new UserRepository());
    final private Map<String, AbstractCommand> commands = new TreeMap<>();
    private User currentUser = null;

    public void init() {
        ConsoleHelper.writeString("Вас приветствует программа Task Manager. " +
                "Наберите \"help\" для вывода списка доступных команд. ");
        String console = "";
        while (!"exit".equals(console)) {
            commands.clear();
            fillCommandMap();
            console = ConsoleHelper.readString();
            AbstractCommand command = commands.get(console);
            if (command != null) {
                command.execute();
            } else {
                if (!"exit".equals(console))
                    ConsoleHelper.writeString("Команда не распознана, введите повторно. Для вызова справки введите \"help\"");
            }
        }
    }

    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @Override
    public UserService getUserService() {
        return userService;
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(User user) {
        currentUser = user;
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return new ArrayList<>(commands.values());
    }

    private void registry(final AbstractCommand command) throws CommandCorruptException {
        final String cliCommand = command.getName();
        final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    private void fillCommandMap() {
        final List<AbstractCommand> currentCommandList = getCurrentCommandList();
        for (AbstractCommand command : currentCommandList
        ) {
            try {
                registry(command);
            } catch (CommandCorruptException e) {
                e.printStackTrace();
            }
        }
    }

    private List<AbstractCommand> getCurrentCommandList() {
        List<AbstractCommand> currentCommandList = new ArrayList<>();
        if (currentUser == null) {
            AbstractCommand[] guestCommands = new AbstractCommand[]{new HelpCommand(), new UserLoginCommand(), new UserRegistryCommand(), new AboutCommand()};
            currentCommandList = Arrays.asList(guestCommands);
        } else {
            switch (currentUser.getRole()) {
                case USER:
                    AbstractCommand[] userCommands = new AbstractCommand[]{new HelpCommand(), new ProjectCreateCommand(),
                            new ProjectListCommand(), new ProjectRemoveCommand(), new ProjectUpdateCommand(),
                            new TaskCreateCommand(), new TaskListByProjectIdCommand(), new TaskListCommand(), new TaskRemoveCommand(),
                            new TaskUpdateCommand(), new UserLogoutCommand(), new UserShowCommand(), new UserUpdatePasswordCommand(),
                            new UserUpdateProfileCommand(), new AboutCommand()};
                    currentCommandList = Arrays.asList(userCommands);
                    break;
                case ADMIN:
                    AbstractCommand[] adminCommands = new AbstractCommand[]{new HelpCommand(), new ProjectClearCommand(), new ProjectCreateCommand(),
                            new ProjectListCommand(), new ProjectRemoveCommand(), new ProjectUpdateCommand(), new TaskClearCommand(),
                            new TaskCreateCommand(), new TaskListByProjectIdCommand(), new TaskListCommand(), new TaskRemoveCommand(),
                            new TaskUpdateCommand(), new UserLogoutCommand(), new UserShowCommand(), new UserUpdatePasswordCommand(),
                            new UserUpdatePasswordCommand(), new AboutCommand()};
                    currentCommandList = Arrays.asList(adminCommands);
                    break;
            }
        }
        return currentCommandList;
    }
}

