package com.morozov.tm.service;

import com.morozov.tm.api.IUserRepository;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserExistException;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.entity.User;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.DataValidation;
import com.morozov.tm.util.MD5Hash;

public final class UserService {

    final private IUserRepository userRepository;

    public UserService(final IUserRepository UserRepository) {
        this.userRepository = UserRepository;
    }

    public User loginUser(final String login, final String password) throws UserNotFoundException, StringEmptyException {
        DataValidation.checkEmptyString(login, password);
        final User user = (User) userRepository.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        final String typePasswordHash = MD5Hash.getHash(password);
        if (typePasswordHash.equals(user.getPasswordHash())) {
            return user;
        } else return null;
    }

    public void registryUser(final String login, final String password) throws UserExistException, StringEmptyException {
        DataValidation.checkEmptyString(login, password);
        if (userRepository.findOneByLogin(login) != null) throw new UserExistException();
        final User user = new User();
        user.setLogin(login);
        user.setRole(UserRoleEnum.USER);
        user.setPasswordHash(MD5Hash.getHash(password));
        userRepository.persist(user.getId(), user);
    }

    public void updateUserPassword(final String id, final String newPassword) throws StringEmptyException {
        final User user = (User) userRepository.findOne(id);
        DataValidation.checkEmptyString(newPassword);
        user.setPasswordHash(MD5Hash.getHash(newPassword));
        userRepository.merge(id, user);
    }

    public void updateUserProfile(final String id, final String newUserName, final String newUserPassword) throws StringEmptyException, UserExistException {
        final User user = (User) userRepository.findOne(id);
        DataValidation.checkEmptyString(newUserName, newUserPassword);
        if (userRepository.findOneByLogin(newUserName) != null) throw new UserExistException();
        user.setLogin(newUserName);
        user.setPasswordHash(MD5Hash.getHash(newUserPassword));
        userRepository.merge(id, user);
    }
}
