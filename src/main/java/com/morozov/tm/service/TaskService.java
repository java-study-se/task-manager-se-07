package com.morozov.tm.service;

import com.morozov.tm.api.ITaskRepository;
import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.util.ConsoleHelper;
import com.morozov.tm.util.DataValidation;

import java.text.ParseException;
import java.util.List;


public final class TaskService {

    final private ITaskRepository taskRepository;

    public TaskService(ITaskRepository TaskRepository) {
        this.taskRepository = TaskRepository;
    }

    public List<Task> getAllTaskByUserId(final String userId) throws RepositoryEmptyException {
        final List<Task> taskList = taskRepository.findAllByUserId(userId);
        DataValidation.checkEmptyRepository(taskList);
        return taskList;
    }

    public Task addTask(final String userId, final String taskName, final String taskDescription, final String dataStart, final String dataEnd, final String projectId) throws StringEmptyException, ParseException {
        DataValidation.checkEmptyString(taskName, taskDescription, dataStart, dataEnd, projectId);
        final Task task = new Task();
        task.setName(taskName);
        task.setDescription(taskDescription);
        task.setStartDate(ConsoleHelper.formattedData(dataStart));
        task.setEndDate(ConsoleHelper.formattedData(dataEnd));
        task.setIdProject(projectId);
        task.setUserId(userId);
        taskRepository.persist(task.getId(), task);
        return task;
    }

    public boolean deleteTask(final String userId, final String id) throws StringEmptyException {
        boolean isDeleted = false;
        DataValidation.checkEmptyString(id);
        if (taskRepository.findOneByUserId(userId, id) != null) {
            taskRepository.remove(id);
            isDeleted = true;
        }
        return isDeleted;
    }

    public void updateTask(final String userId, final String id, final String name, final String description, final String startDate, final String endDate, final String projectId) throws RepositoryEmptyException, StringEmptyException, ParseException {
        DataValidation.checkEmptyRepository(taskRepository.findAll());
        DataValidation.checkEmptyString(id, name, description, startDate, endDate, projectId);
        final Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setStartDate(ConsoleHelper.formattedData(startDate));
        task.setEndDate(ConsoleHelper.formattedData(endDate));
        task.setIdProject(projectId);
        task.setUserId(userId);
        taskRepository.merge(id, task);
    }

    public List<Task> getAllTaskByProjectId(final String userId, final String projectId) throws StringEmptyException, RepositoryEmptyException {
        DataValidation.checkEmptyString(projectId);
        final List<Task> resultTaskList = taskRepository.findAllByProjectIdUserId(userId, projectId);
        DataValidation.checkEmptyRepository(resultTaskList);
        return resultTaskList;
    }

    public void deleteAllTaskByProjectId(final String userId, final String projectId) {
        taskRepository.deleteAllTaskByProjectId(userId, projectId);
    }

    public void clearTaskList() {
        taskRepository.removeAll();
    }
}
