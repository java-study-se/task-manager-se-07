package com.morozov.tm.service;

import com.morozov.tm.api.IProjectRepository;
import com.morozov.tm.entity.Project;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.util.ConsoleHelper;
import com.morozov.tm.util.DataValidation;

import java.text.ParseException;
import java.util.List;

public final class ProjectService {

    final private IProjectRepository projectRepository;

    public ProjectService(IProjectRepository ProjectRepository) {
        this.projectRepository = ProjectRepository;
    }

    public final List<Project> getAllProject() throws RepositoryEmptyException {
        final List<Project> projectList = projectRepository.findAll();
        DataValidation.checkEmptyRepository(projectList);
        return projectList;
    }
    public final List<Project> getAllProjectByUserId(String userId) throws RepositoryEmptyException {
        final List<Project> projectListByUserId = projectRepository.findAllByUserId(userId);
        DataValidation.checkEmptyRepository(projectListByUserId);
        return projectListByUserId;
    }

    public final Project addProject(String userId, String projectName, String projectDescription, String dataStart, String dataEnd) throws StringEmptyException, ParseException {
        DataValidation.checkEmptyString(projectName, projectDescription, dataStart, dataEnd);
        final Project project = new Project();
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setStartDate(ConsoleHelper.formattedData(dataStart));
        project.setEndDate(ConsoleHelper.formattedData(dataEnd));
        project.setUserId(userId);
        projectRepository.persist(project.getId(), project);
        return project;
    }

    public final boolean deleteProject(String userId, String id) throws StringEmptyException {
        boolean isDeleted = false;
        DataValidation.checkEmptyString(id);
        if (projectRepository.findOneByUserId(userId, id) != null) {
            projectRepository.remove(id);
            isDeleted = true;
        }
        return isDeleted;
    }

    public final void updateProject(String userId, String id, String projectName, String projectDescription, String dataStart, String dataEnd) throws RepositoryEmptyException, StringEmptyException, ParseException {
        DataValidation.checkEmptyRepository(projectRepository.findAllByUserId(userId));
        DataValidation.checkEmptyString(id, projectName, projectDescription, dataStart, dataEnd);
        final Project project = new Project();
        project.setId(id);
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setStartDate(ConsoleHelper.formattedData(dataStart));
        project.setEndDate(ConsoleHelper.formattedData(dataEnd));
        project.setUserId(userId);
        projectRepository.merge(id, project);
    }

    public final void clearProjectList() {
        projectRepository.removeAll();
    }

}
