package com.morozov.tm.repository;

import com.morozov.tm.api.IProjectRepository;
import com.morozov.tm.entity.Project;
import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository<Project> {

    @Override
     public List<Project> findAllByUserId(final String userId){
        final List<Project> projectListByUserID = new ArrayList<>();
        for (Project project: findAll()) {
            if(project.getUserId().equals(userId)) projectListByUserID.add(project);
        }
        return projectListByUserID;
    }

    @Override
    public Project findOneByUserId(final String userId, final String id){
        Project resultProject = null;
        for (Project project : findAllByUserId(userId)) {
            if(project.getId().equals(id)) resultProject = project;
        }
        return resultProject;
    }
}
