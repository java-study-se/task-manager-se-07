package com.morozov.tm.repository;

import com.morozov.tm.api.ITaskRepository;
import com.morozov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository<Task> {


    @Override
    public List<Task> findAllByProjectIdUserId(String userId, String projectId) {
        final List<Task> resultListByUserId = new ArrayList<>();
        for (Task task : getAllTaskByProjectId(projectId)) {
            if (task.getUserId().equals(userId)) resultListByUserId.add(task);
        }
        return resultListByUserId;
    }

    @Override
    public List<Task> findAllByUserId(String userId) {
        final List<Task> resultTaskList = new ArrayList<>();
        for (Task task : findAll()) {
            if (task.getUserId().equals(userId)) resultTaskList.add(task);
        }
        return resultTaskList;
    }

    @Override
    public Task findOneByUserId(String userId, String id) {
        Task resultTask = null;
        for (Task task : findAllByUserId(userId)) {
            if (task.getId().equals(id)) resultTask = task;
        }
        return resultTask;
    }

    @Override
    public List<Task> getAllTaskByProjectId(String projectId) {
        List<Task> resultList = new ArrayList<>();
        for (Task task : entityMap.values()) {
            if (task.getIdProject().equals(projectId)) resultList.add(task);
        }
        return resultList;
    }

    @Override
    public void deleteAllTaskByProjectId(String userId, String projectId) {
        final List<Task> taskToDelete = findAllByProjectIdUserId(userId, projectId);
        for (Task task : taskToDelete) {
            remove(task.getId());
        }
    }

}
