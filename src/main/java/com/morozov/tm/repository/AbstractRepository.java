package com.morozov.tm.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<T> {
    final Map<String, T> entityMap = new HashMap<>();

    public List<T> findAll() {
        final List<T> userList = new ArrayList<>();
        userList.addAll(entityMap.values());
        return userList;
    }

    public T findOne(String id) {
        return entityMap.get(id);
    }

    public void merge(String id, T updateEntity) {
        entityMap.put(id, updateEntity);
    }

    public void persist(String id, T writeEntity) {
        entityMap.put(id, writeEntity);
    }

    public void remove(String id) {
        entityMap.remove(id);
    }

    public void removeAll() {
        entityMap.clear();
    }
}
