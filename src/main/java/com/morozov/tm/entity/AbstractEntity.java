package com.morozov.tm.entity;

import java.util.UUID;

public class AbstractEntity {
    protected String id = UUID.randomUUID().toString();

    final public String getId() {
        return id;
    }

    final public void setId(String id) {
        this.id = id;
    }
}
