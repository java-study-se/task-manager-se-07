package com.morozov.tm.entity;

import java.util.Date;
import java.util.UUID;

public class Task extends AbstractEntity{
    private String name = "";
    private String description = "";
    private Date startDate = null;
    private Date endDate = null;
    private String idProject = "";
    private String userId = "";

    final public String getName() {
        return name;
    }

    final public void setName(String name) {
        this.name = name;
    }

    final public String getDescription() {
        return description;
    }

    final public void setDescription(String description) {
        this.description = description;
    }

    final public Date getStartDate() {
        return startDate;
    }

    final public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    final public Date getEndDate() {
        return endDate;
    }

    final public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    final public String getIdProject() {
        return idProject;
    }

    final public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    final public String getUserId() {
        return userId;
    }

    final public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    final public String toString() {
        return "Task{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", idProject='" + idProject + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
