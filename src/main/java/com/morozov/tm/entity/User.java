package com.morozov.tm.entity;

import com.morozov.tm.security.role.UserRoleEnum;

import java.util.UUID;

public class User extends AbstractEntity{
    private String login = "";
    private String passwordHash = "";
    private UserRoleEnum role = UserRoleEnum.USER;

    final public String getLogin() {
        return login;
    }

    final public void setLogin(String login) {
        this.login = login;
    }

    final public UserRoleEnum getRole() {
        return role;
    }

    final public void setRole(UserRoleEnum role) {
        this.role = role;
    }

    final public String getPasswordHash() {
        return passwordHash;
    }

    final public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
}
